import csv
import datetime
import os.path

class ScoreToCSV():
    """импортирует рекорд в csv"""
    def __init__(self):

        # начальное значение текущего рекорд в текущей игре
        self.current_game_score = 0

        # проверить наличие файла scorelist.csv
        self._check_file()


    def check_scores_in_list(self):
        """"проверяет является ли текущий рекорд большим предыдущих и вносить новый рекорд"""
        print("проверка рекорда")
        with open('scorelist.csv',  'r', newline='') as csvfile:
            csv_file = csv.reader(csvfile, delimiter='/')
            past_records_list = []  # список сохраненных рекордов
            for i in csv_file:   # заполнить список сохраненных рекордов
                past_records_list.append(i[0])
            past_records_list.sort()  # отсортировать список сохраненных рекордов

            if past_records_list:   # если список сохраненных рекордов не пуст
                if self.current_game_score > int(past_records_list[-1]):
                # является ли текущий рекорд новым рекордом в статистике?
                    self.add_new_record()
                    print('New record!')
            else:  # если список сохраненных рекордов пуст
                self.add_new_record()
                print('First record!')


    def add_new_record(self):
        """добавляет новый рекорд в records.csv"""
        with open('scorelist.csv', 'a', newline='') as csvfile:
            csv_file = csv.writer(csvfile, delimiter='/')
            now = datetime.datetime.now()
            csv_file.writerow([self.current_game_score, now.strftime("%d-%m-%Y %H:%M")])


    def _check_file(self):
        """проверяет существование файсла records.csv"""
        if not os.path.exists("scorelist.csv"):
            with open('scorelist.csv', 'w', newline='') as csvfile:
                pass
            print('file scorelist.csv created!')
        else:
            print('file scorelist.csv exists!')


