import sys
import pygame
from settings import Settings
from ship import Ship




class AlienInvasion:
    """Класс для управления ресурсами и поведением игры."""

    def __init__(self):
        """Инициализирует игру и создает игровые ресурсы."""
        pygame.init()
        self.settings = Settings()  # создает для настроек экземплят импорт-го класса Settings

        self.screen = pygame.display.set_mode(
            (self.settings.screen_width, self.settings.screen_height))  # задает размер окна (поверхность)  - берет из Settings
        pygame.display.set_caption("Alien Invasion")

        self.ship = Ship(self)    # При вызове Ship передается один аргумент — экземпляр AlienInvasion  (p248)


        # Назначение цвета фона.
        self.bg_color = (self.settings.bg_color)  # - берет из Settings

    def run_game(self):
        """Запуск основного цикла игры."""
        while True:  # Отслеживание событий клавиатуры и мыши.
            self._check_events()
            self._update_screen()


    def _check_events(self):
        """Обрабатывает нажатия клавиш и события мыши."""
        for event in pygame.event.get():  # цикл событий (действий пользователя)
            # для прослушивания событий и выполнения операции
            if event.type == pygame.QUIT:
                sys.exit()

    def _update_screen(self):
        """Обновляет изображения на экране и отображает новый экран."""
        self.screen.fill(self.bg_color)  # заполняет экран цветом фона
        self.ship.blitme()

        # Отображение последнего прорисованного экрана.
        pygame.display.flip()


if __name__ == '__main__':  # Создание экземпляра и запуск игры.
    ai = AlienInvasion()
    ai.run_game()