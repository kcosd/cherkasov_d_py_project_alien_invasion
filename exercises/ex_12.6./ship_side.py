import pygame

class Ship():
    """Класс для управления кораблем."""

    def __init__(self, ai_game):
        """Инициализирует корабль и задает его начальную позицию."""

        self.screen = ai_game.screen
        self.settings = ai_game.settings
        self.screen_rect = ai_game.screen.get_rect()

        # Загружает изображение корабля и получает прямоугольник.
        self.image = pygame.image.load('images/mig_side.png')
        self.rect = self.image.get_rect()

        # Каждый новый корабль появляется у нижнего края экрана.
        self.rect.midright = self.screen_rect.midright

        # Сохранение вещественной координаты центра корабля.
        self.x = float(self.rect.x)
        self.y = float(self.rect.y)

        # Флаг перемещения
        self.moving_right = False
        self.moving_left = False
        self.moving_up = False
        self.moving_down = False

    def update(self):
        """Обновляет позицию корабля с учетом флага."""
        # Обновляется атрибут x, не rect.
        if self.moving_right and self.rect.right < self.screen_rect.right:   # ВПРАВО и положение прав.края корабля меньше правого края экрана
            self.x += self.settings.ship_speed  # объект кораболь смещен вправо по оси x (количество пикселей в ttings.ship_speed)

        if self.moving_left and self.rect.left > 0:   # ВЛЕВО и положение лев.края корабля меньше правого края экрана
            self.x -= self.settings.ship_speed   # объект кораболь смещен влево по оси x (количество пикселей в ttings.ship_speed)

        if self.moving_up and self.rect.top > 0:   # ВВЕРХ
            self.y -= self.settings.ship_speed   # объект кораболь смещен вверх по оси y (количество пикселей в ttings.ship_speed)

        if self.moving_down and self.rect.bottom < self.screen_rect.bottom:   # ВНИЗ
            self.y += self.settings.ship_speed   # объект кораболь смещен вниз по оси y (количество пикселей в ttings.ship_speed)


        # Обновление атрибута rect на основании self.x.
        self.rect.x = self.x
        self.rect.y = self.y


    def blitme(self):
        """Рисует корабль в текущей позиции."""
        self.screen.blit(self.image, self.rect)


