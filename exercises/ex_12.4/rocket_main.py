import sys
import pygame
from settings import Settings
from ship import Ship


class Rocket:
    """Класс для управления ресурсами и поведением игры."""

    def __init__(self):
        """Инициализирует игру и создает игровые ресурсы."""
        pygame.init()
        self.settings = Settings()  # создает для настроек экземплят импорт-го класса Settings

        self.screen = pygame.display.set_mode(
            (self.settings.screen_width, self.settings.screen_height)) # задает размер окна (поверхность)


        # для запуска в окне значение self.screen должно быть:
        # pygame.display.set_mode((self.settings.screen_width, self.settings.screen_height))

        pygame.display.set_caption("Rocket")






        self.ship = Ship(self)    # При вызове Ship передается один аргумент — экземпляр AlienInvasion  (p248)


        # Назначение цвета фона.
        self.bg_color = (self.settings.bg_color)  # - берет из Settings

    def run_game(self):
        """Запуск основного цикла игры."""
        while True:  # Отслеживание событий клавиатуры и мыши.
            self._check_events()
            self.ship.update()  # обновляет позицию корабля после проверки событий клавиатуры, но перед обновлением экрана.
            self._update_screen()


    def _check_events(self):
        """Обрабатывает нажатия клавиш и события мыши."""
        for event in pygame.event.get():  # цикл событий (действий пользователя)
            # для прослушивания событий и выполнения операции
            if event.type == pygame.QUIT:
                sys.exit()

            elif event.type == pygame.KEYDOWN:  # условие - нажатие клавиши
                self._check_keydown_events(event)

            elif event.type == pygame.KEYUP:  # при отпускании клавиши флаг в классе Ship снова меняется на False для остановки
                self._check_keyup_events(event)


    def _check_keydown_events(self, event):
        """Реагирует на нажатие клавиш."""
        if event.key == pygame.K_RIGHT:  # условие - если нажата клавиша ВПРАВО
            # Переместить корабль вправо.
            self.ship.moving_right = True  # меняет флаг в классе Ship для перемещения
        elif event.key == pygame.K_LEFT:  # условие - если нажата клавиша ВЛЕВО
            self.ship.moving_left = True
        elif event.key == pygame.K_q:  # клавиша q для выхода (реагирует только на англ. раскладке, русский - не реагирует)
            sys.exit()
        elif event.key == pygame.K_UP:  # условие - если нажата клавиша ВВЕРХ
            self.ship.moving_up = True
        elif event.key == pygame.K_DOWN:  # условие - если нажата клавиша ВНИЗ
            self.ship.moving_down = True

    def _check_keyup_events(self, event):
        """Реагирует на отпускание клавиш."""
        if event.key == pygame.K_RIGHT:  # условие - если отпущена клавиша ВПРАВО
            self.ship.moving_right = False
        elif event.key == pygame.K_LEFT:  # условие - если отпущена клавиша ВЛЕВО
            self.ship.moving_left = False
        elif event.key == pygame.K_UP:  # условие - если отпущена клавиша ВВЕРХ
            self.ship.moving_up = False
        elif event.key == pygame.K_DOWN:  # условие - если отпущена клавиша ВНИЗ
            self.ship.moving_down = False



    def _update_screen(self):
        """Обновляет изображения на экране и отображает новый экран."""
        self.screen.fill(self.bg_color)  # заполняет экран цветом фона
        self.ship.blitme()

        # Отображение последнего прорисованного экрана.
        pygame.display.flip()


if __name__ == '__main__':  # Создание экземпляра и запуск игры.
    ai = Rocket()
    ai.run_game()